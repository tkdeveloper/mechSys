package system.services;

import system.models.Order;
import java.util.List;

public interface PostService {
    List<Order> findAll();
    Order findById(Long id);
    Order create(Order post);
    Order edit(Order post);
    void deleteById(Long id);
}
