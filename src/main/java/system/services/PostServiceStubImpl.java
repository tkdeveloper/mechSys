package system.services;


import org.springframework.stereotype.Service;
import system.models.Order;

import java.util.*;

@Service
public class PostServiceStubImpl implements PostService {
    private List<Order> orderList = new ArrayList() {{
        add(new Order(1L, "Tyre", "Remove and change tyre", System.currentTimeMillis()));
    }};

    @Override
    public List findAll() {
        return this.orderList;
    }

    @Override
    public Order findById(Long id) {
        return orderList.stream()
                .filter(p -> Objects.equals(p.getId(), id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Order create(Order order) {
        order.setId(orderList.stream().mapToLong(Order::getId).max().getAsLong() + 1);
        orderList.add(order);
        return order;
    }

    @Override
    public Order edit(Order order) {
        for (int i = 0; i < orderList.size(); i++) {
            if (Objects.equals(orderList.get(i).getId(), order.getId())) {
                orderList.set(i, order);
                return order;
            }
        }
        throw new RuntimeException("Order not found" + order.getId());
    }

    @Override
    public void deleteById(Long id) {
        for(int i = 0; i < orderList.size(); i++) {
            if(Objects.equals(orderList.get(i).getId(), id)) {
                orderList.remove(i);
            }
        }
    }
}
