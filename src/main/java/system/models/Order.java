package system.models;

import java.util.Date;

public class Order {
    private Long id;
    private String title;
    private String description;
    private Long orderedDate;

    public Order(Long id, String title, String description, Long orderedDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.orderedDate = orderedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getOrderedDate() {
        return orderedDate;
    }

    public void setOrderedDate(Long orderedDate) {
        this.orderedDate = orderedDate;
    }
}
